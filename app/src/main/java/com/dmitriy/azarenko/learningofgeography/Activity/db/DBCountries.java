package com.dmitriy.azarenko.learningofgeography.Activity.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;


public class DBCountries extends SQLiteOpenHelper {

    private static final String LOG_TAG = "myLogs";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "countriesDB";


    public static class CountriesColumns implements BaseColumns {
        public static final String TABLE_COUNTRIES = "countries";
        public static final String KEY_NAME = "name";
        public static final String KEY_CAPITAL = "capital";
        public static final String KEY_REGION = "region";
        public static final String KEY_SUBREGION = "subregion";

    }

    private static String CREATE_TABLE_COUNTRIES = "CREATE TABLE " + CountriesColumns.TABLE_COUNTRIES +
            " (" +
            CountriesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CountriesColumns.KEY_NAME + " TEXT," +
            CountriesColumns.KEY_REGION + " TEXT," +
            CountriesColumns.KEY_SUBREGION + " TEXT," +
            CountriesColumns.KEY_CAPITAL + " TEXT" +
            ");";


    public DBCountries(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_COUNTRIES);
        Log.d(LOG_TAG, "Creating countries DB");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
