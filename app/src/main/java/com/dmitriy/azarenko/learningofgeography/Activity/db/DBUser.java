package com.dmitriy.azarenko.learningofgeography.Activity.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DBUser extends SQLiteOpenHelper {

    private static final String LOG_TAG = "myLogs";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "userDB";


    public static class UserColumns implements BaseColumns {
        public static final String TABLE_USERS = "users";
        public static final String KEY_LOGIN = "login";
        public static final String KEY_PASSWORD = "password";
        public static final String KEY_COUNTRY_OF_USER = "country";


    }

    private static String CREATE_TABLE_USERS = "CREATE TABLE " + UserColumns.TABLE_USERS +
            " (" +
            UserColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            UserColumns.KEY_LOGIN + " TEXT," +
            UserColumns.KEY_PASSWORD + " TEXT," +
            UserColumns.KEY_COUNTRY_OF_USER + " TEXT" +

            ");";


    public DBUser(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERS);
        Log.d(LOG_TAG, "Creating Users DB");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
