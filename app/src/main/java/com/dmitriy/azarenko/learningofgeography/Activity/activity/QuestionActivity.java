package com.dmitriy.azarenko.learningofgeography.Activity.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountriesMaster;
import com.dmitriy.azarenko.learningofgeography.Activity.db.DBUser;
import com.dmitriy.azarenko.learningofgeography.R;

import java.util.List;

public class QuestionActivity extends AppCompatActivity {

    private static final String LOG_TAG = "myLogs";

    TextView tvQuestion;
    EditText etAnswer;
    Button btCheck;
    DBUser dbUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        DBCountriesMaster db = new DBCountriesMaster(getApplicationContext());
        List<String> countryNames = db.getAllCountryNames();

        dbUser = new DBUser(this);

        SQLiteDatabase database = dbUser.getWritableDatabase();

        Cursor cursor = database.query(DBUser.UserColumns.TABLE_USERS, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            int loginIndex = cursor.getColumnIndex(DBUser.UserColumns.KEY_COUNTRY_OF_USER);

            do {

                if (String.valueOf(countryNames).equals(cursor.getString(loginIndex))) {

                    Toast.makeText(getApplicationContext(), "Yout country is: " + loginIndex, Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(getApplicationContext(), "Something wrong ", Toast.LENGTH_SHORT).show();


                }


            } while (cursor.moveToNext());
        } else


            cursor.close();
        dbUser.close();


    }


}
