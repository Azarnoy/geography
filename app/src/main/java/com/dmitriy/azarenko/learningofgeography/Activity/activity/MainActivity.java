package com.dmitriy.azarenko.learningofgeography.Activity.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountriesMaster;
import com.dmitriy.azarenko.learningofgeography.Activity.db.DBUser;
import com.dmitriy.azarenko.learningofgeography.Activity.modules.Countries;
import com.dmitriy.azarenko.learningofgeography.Activity.modules.Retrofit;
import com.dmitriy.azarenko.learningofgeography.R;
import com.facebook.stetho.Stetho;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "myLogs";

    EditText etLogin;
    EditText etPassword;
    Button btLogin;
    Button btReg;

    DBUser dbUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registration();
        login();
        dbUser = new DBUser(this);
        Stetho.initializeWithDefaults(this);

        List<Countries> query = DBCountriesMaster.getInstance(this).getAllCountriesQuery();

        if (query.isEmpty()) {
            Retrofit.getCountries(new Callback<List<Countries>>() {
                @Override
                public void success(List<Countries> countries, Response response) {
                    Toast.makeText(getApplicationContext(), "Waiting for loading data", Toast.LENGTH_LONG).show();
                    Log.d(LOG_TAG, "Uploading data from Retrofit");

                    for (Countries country : countries) {
                        DBCountriesMaster.getInstance(MainActivity.this).addCountry(country);
                    }
                    Log.d(LOG_TAG, "Data from Retrofit saved in CountryDB");

                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

    private void login() {
        btLogin = (Button) findViewById(R.id.button_for_login);
        etLogin = (EditText) findViewById(R.id.enter_login_id);
        etPassword = (EditText) findViewById(R.id.enter_password_loginAt_id);


        btLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String login = etLogin.getText().toString();
                String password = etPassword.getText().toString();

                SQLiteDatabase database = dbUser.getWritableDatabase();

                Cursor cursor = database.query(DBUser.UserColumns.TABLE_USERS, null, null, null, null, null, null);

                if (cursor.moveToFirst()) {

                    int loginIndex = cursor.getColumnIndex(DBUser.UserColumns.KEY_LOGIN);
                    int passwordIndex = cursor.getColumnIndex(DBUser.UserColumns.KEY_PASSWORD);
                    do {

                        if (login.equals(cursor.getString(loginIndex)) && password.equals(cursor.getString(passwordIndex))) {
                            Intent intent = new Intent(MainActivity.this, QuestionActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), "Login or password Incorrect", Toast.LENGTH_SHORT).show();
                        }


                    } while (cursor.moveToNext());
                } else


                    cursor.close();
                dbUser.close();


            }
        });
    }

    private void registration() {
        btReg = (Button) findViewById(R.id.button_for_registration);
        btReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
                startActivity(intent);
                Log.d(LOG_TAG, "Start Registration Activity...(Registration Button");

            }
        });
    }
}
