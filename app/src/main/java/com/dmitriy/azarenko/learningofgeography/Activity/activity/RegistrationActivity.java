package com.dmitriy.azarenko.learningofgeography.Activity.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountriesMaster;
import com.dmitriy.azarenko.learningofgeography.Activity.db.DBUser;
import com.dmitriy.azarenko.learningofgeography.Activity.modules.Countries;
import com.dmitriy.azarenko.learningofgeography.R;
import com.facebook.stetho.Stetho;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity {

    private static final String LOG_TAG = "myLogs";

    EditText etLogin;
    EditText etPassword;
    EditText etCountry;
    Button btSubmit;
    Spinner spinner;

    DBUser dbUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Log.d(LOG_TAG, "Registration Activity is open");


        etLogin = (EditText) findViewById(R.id.enter_login_reg);
        etPassword = (EditText) findViewById(R.id.enter_password_reg);
//        etCountry = (EditText) findViewById(R.id.enter_country_reg);
        btSubmit = (Button) findViewById(R.id.button_for_reg);
        spinner = (Spinner) findViewById(R.id.spinner);

        loadSpinnerData();

        List<Countries> query = DBCountriesMaster.getInstance(this).getAllCountriesQuery();

        dbUser = new DBUser(this);


        Stetho.initializeWithDefaults(this);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etLogin.getText().toString();
                String password = etPassword.getText().toString();
                String selected = spinner.getSelectedItem().toString();

                SQLiteDatabase database = dbUser.getWritableDatabase();

                ContentValues contentValues = new ContentValues();

                String upperChar = "([A-Z])";
                Pattern pattern = Pattern.compile(upperChar);
                Matcher matcher = pattern.matcher(etPassword.getText().toString());

                if (etLogin.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Login is empty", Toast.LENGTH_SHORT).show();


                } else if (etPassword.getText().length() <= 5) {
                    Toast.makeText(getApplicationContext(), "Password is too short", Toast.LENGTH_SHORT).show();

                } else if (!matcher.find()) {
                    Toast.makeText(getApplicationContext(), "Password must have at least one big letter", Toast.LENGTH_SHORT).show();

                } else {
                    contentValues.put(DBUser.UserColumns.KEY_LOGIN, name);
                    contentValues.put(DBUser.UserColumns.KEY_PASSWORD, password);
                    contentValues.put(DBUser.UserColumns.KEY_COUNTRY_OF_USER, selected);
                    database.insert(DBUser.UserColumns.TABLE_USERS, null, contentValues);

                    Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                Log.d(LOG_TAG, "Press Button Submit");
                Log.d(LOG_TAG, "User data saved in UsersDB");
                Log.d(LOG_TAG, "Open Login menui");

            }


        });


    }

    //     Method for attach data to spinner from CountriesDB
    private void loadSpinnerData() {
        DBCountriesMaster db = new DBCountriesMaster(getApplicationContext());
        List<String> countryNames = db.getAllCountryNames();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryNames);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        Log.d(LOG_TAG, "attaching data to spinner");
    }


}
