package com.dmitriy.azarenko.learningofgeography.Activity.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns;
import com.dmitriy.azarenko.learningofgeography.Activity.modules.Countries;

import java.util.ArrayList;
import java.util.List;

import static com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns.KEY_CAPITAL;
import static com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns.KEY_NAME;
import static com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns.KEY_REGION;
import static com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns.KEY_SUBREGION;
import static com.dmitriy.azarenko.learningofgeography.Activity.db.DBCountries.CountriesColumns.TABLE_COUNTRIES;


public class DBCountriesMaster {

    private static final String LOG_TAG = "myLogs";

    public SQLiteDatabase database;
    public DBCountries dbCountries;

    public static DBCountriesMaster instance;


    public DBCountriesMaster(Context context) {
        dbCountries = new DBCountries(context);
        if (database == null || !database.isOpen()) {
            database = dbCountries.getWritableDatabase();
        }

    }

    public static DBCountriesMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DBCountriesMaster(context);
        }
        return instance;
    }

    public void addCountry(Countries countries) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, countries.name);
        cv.put(KEY_CAPITAL, countries.capital);
        cv.put(KEY_REGION, countries.region);
        cv.put(KEY_SUBREGION, countries.subregion);


        database.insert(TABLE_COUNTRIES, null, cv);
//        DataBaseCreator.UserColums  alt+enter and do static, and after i use just constants from DBCountries;


    }

    public List<Countries> getAllCountriesQuery() {

        Cursor cursor = database.query(CountriesColumns.TABLE_COUNTRIES, null, null, null,
                null, null, null);

        List<Countries> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Countries countries = new Countries();
            countries.name = cursor.getString(cursor.getColumnIndex(CountriesColumns.KEY_NAME));

            list.add(countries);
            cursor.moveToNext();

        }
        cursor.close();
        return list;
    }

    public List<String> getAllCountryNames() {
        List<String> list = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_COUNTRIES;

        SQLiteDatabase db = dbCountries.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(1));//adding 2nd column data
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();

        // returning list of Countries
        return list;
    }


}
