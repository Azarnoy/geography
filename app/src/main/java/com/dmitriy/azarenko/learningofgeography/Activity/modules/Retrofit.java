package com.dmitriy.azarenko.learningofgeography.Activity.modules;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;


public class Retrofit {

    private static final String LOG_TAG = "myLogs";

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/all")
        void getCountries(Callback<List<Countries>> callback);


    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<Countries>> callback) {
        apiInterface.getCountries(callback);
    }


}